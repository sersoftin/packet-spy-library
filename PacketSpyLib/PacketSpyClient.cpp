#include "PacketSpyClient.h"



PacketSpyClient::PacketSpyClient()
{
	incomingTrafficClient_ = new Client(L"incoming_pipe");
	outgoingTrafficClient_ = new Client(L"outgoing_pipe");
}

bool PacketSpyClient::SendOutgoingPacket(unsigned char * buffer_data, size_t buffer_data_len)
{
	auto written_data = outgoingTrafficClient_->Write(buffer_data, buffer_data_len);
	return written_data == buffer_data_len;
}

bool PacketSpyClient::ReceiveOutgoingPacket(unsigned char * buffer_data, size_t * buffer_data_len)
{
	auto read_data = outgoingTrafficClient_->Read(buffer_data, buffer_data_len);
	*buffer_data_len = read_data;
	return read_data > 0;
}

bool PacketSpyClient::SendIncomingPacket(unsigned char * buffer_data, size_t buffer_data_len)
{
	auto written_data = incomingTrafficClient_->Write(buffer_data, buffer_data_len);
	return written_data == buffer_data_len;
}

bool PacketSpyClient::ReceiveIncomingPacket(unsigned char * buffer_data, size_t * buffer_data_len)
{
	auto read_data = incomingTrafficClient_->Read(buffer_data, buffer_data_len);
	*buffer_data_len = read_data;
	return read_data > 0;
}


PacketSpyClient::~PacketSpyClient()
{
	delete incomingTrafficClient_;
	delete outgoingTrafficClient_;
}
