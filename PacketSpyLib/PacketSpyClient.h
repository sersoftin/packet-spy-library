#pragma once
#include "Client.h"

class PacketSpyClient
{
public:
	PacketSpyClient();


	bool SendOutgoingPacket(unsigned char * buffer_data, size_t buffer_data_len); // �������� ���������� ������ � GUI, ����������� ��������
	bool ReceiveOutgoingPacket(unsigned char * buffer_data, size_t * buffer_data_len); // ��������� ���������� ������ �� GUI, ����������� ��������

	bool SendIncomingPacket(unsigned char * buffer_data, size_t buffer_data_len); // �������� ��������� ������ � GUI, ����������� ��������
	bool ReceiveIncomingPacket(unsigned char * buffer_data, size_t * buffer_data_len); // ��������� ��������� ������ �� GUI, ����������� ��������


	~PacketSpyClient();

private:
	Client * incomingTrafficClient_ = nullptr;
	Client * outgoingTrafficClient_ = nullptr;
};