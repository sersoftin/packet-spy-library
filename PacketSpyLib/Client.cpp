#include "Client.h"


Client::Client(std::wstring pipe_name)
{
	std::wstring pipe_name_local = L"\\\\.\\pipe\\" + pipe_name;
	pipe_handle_ = CreateFileW(pipe_name_local.c_str(), GENERIC_READ | GENERIC_WRITE, 0, nullptr, OPEN_EXISTING, 0, nullptr);
	if (pipe_handle_ == INVALID_HANDLE_VALUE)
		throw exceptions::NamedPipeConnectionException();
	DWORD mode = PIPE_READMODE_MESSAGE | PIPE_WAIT;
	SetNamedPipeHandleState(pipe_handle_, &mode, NULL, NULL);
}

Client::Client(std::string pipe_name)
{
	std::string pipe_name_local = "\\\\.\\pipe\\" + pipe_name;
	pipe_handle_ = CreateFileA(pipe_name_local.c_str(), GENERIC_READ | GENERIC_WRITE, 0, nullptr, OPEN_EXISTING, 0, nullptr);
	if (pipe_handle_ == INVALID_HANDLE_VALUE)
		throw exceptions::NamedPipeConnectionException();
	DWORD mode = PIPE_READMODE_MESSAGE | PIPE_WAIT;
	SetNamedPipeHandleState(pipe_handle_, &mode, NULL, NULL);
}


size_t Client::Write(unsigned char * buffer_data, size_t buffer_data_len)
{
	DWORD bytesWritten = 0;
	if (!WriteFile(pipe_handle_, buffer_data, buffer_data_len, &bytesWritten, NULL))
		throw exceptions::NamedPipeIOException();
	return bytesWritten;
}

size_t Client::Read(unsigned char * buffer_data, size_t * buffer_data_len)
{
	DWORD bytesRead = 0;
	if (!ReadFile(pipe_handle_, buffer_data, *buffer_data_len, &bytesRead, NULL))
		throw exceptions::NamedPipeIOException();
	return bytesRead;
}

Client::~Client()
{
	CloseHandle(pipe_handle_);
}
