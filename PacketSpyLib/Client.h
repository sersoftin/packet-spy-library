#pragma once
#include <iostream>
#include <string>
#include <Windows.h>
#include "NamedPipeIOException.h"
#include "NamedPipeConnectionException.h"

class Client
{
public:
	Client(std::wstring pipe_name);
	Client(std::string pipe_name);

	size_t Write(unsigned char * buffer_data, size_t buffer_data_len); // ������ ������ � �����
	size_t Read(unsigned char * buffer_data, size_t * buffer_data_len); // ������ ������ �� �����

	~Client();

private:
	HANDLE pipe_handle_ = nullptr;
};

