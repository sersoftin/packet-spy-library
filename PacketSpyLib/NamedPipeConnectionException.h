#pragma once
#include <exception>

namespace exceptions
{
	class NamedPipeConnectionException : std::exception
	{
	public:
		NamedPipeConnectionException();
		~NamedPipeConnectionException();
	};
}