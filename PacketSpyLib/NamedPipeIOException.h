#pragma once
#include <exception>

namespace exceptions
{
	class NamedPipeIOException : std::exception
	{
	public:
		NamedPipeIOException();
		~NamedPipeIOException();
	};
}